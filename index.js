
const express = require('express')

const dotenv = require('dotenv')

const mongoose = require('mongoose')

const app = express()

const port = 3001

dotenv.config()
mongoose.connect(`mongodb+srv://zuittdiscussion:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.xbtkvqt.mongodb.net/S35-Activity?retryWrites=true&w=majority`, {
	useNewUrlParser : true,
	useUnifiedTopology : true
	}
)
let db = mongoose.connection
db.on('error', console.error.bind(console, "Connection Error"))
db.on('open', () => console.log('Connected to MongoDB!'))

//Schema

const userSchema = new mongoose.Schema({
	username : String,
	password : String
})

//Model

const User = mongoose.model('User', userSchema)

//Middleware first
app.use(express.json()) 
app.use(express.urlencoded({extended: true}))


//Routes

//POST register



app.post('/signup', (req,res) => {
	User.findOne({username: req.body.username}, (error,result) =>{
		if(result != null && result.username == req.body.username){
			return res.send('Duplicate username found!')
		}else{
			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			})
			newUser.save((error, savedUser) => {
				if(error){
					return console.error(error)
				} return res.status(250).send('New user registered!')
			})
		}
	})
})




app.listen(port, () => console.log(`Server is running at port ${port}`))